package listas;

public class Ejecutable {
    public static  void main(String args[]){
        ListaEnlazada listaEnlazada = new ListaEnlazada();
        listaEnlazada.agregarInicio(2);
        listaEnlazada.agregarInicio(4);
        listaEnlazada.agregarInicio(8);
        listaEnlazada.agregarInicio(16);
        listaEnlazada.agregarInicio(32);
        listaEnlazada.agregarFinal(1);
        listaEnlazada.verLista();
        listaEnlazada.eliminar(1);
        listaEnlazada.eliminar(32);
        listaEnlazada.verLista();
        System.out.println("Lista enlazadada ordenada...");
        ListaEnlazadaOrdenada listaOrdenada = new ListaEnlazadaOrdenada();
        listaOrdenada.insertar(1);
        listaOrdenada.insertar(2);
        listaOrdenada.insertar(13);
        listaOrdenada.insertar(3);
        listaOrdenada.insertar(8);
        listaOrdenada.insertar(5);
        listaOrdenada.insertar(1);
        listaOrdenada.insertar(21);
        listaOrdenada.verLista();
        //
        Nodo encontrado = listaEnlazada.buscar(8);
        if(encontrado != null) {
            System.out.println("El dato encontrado es: " + encontrado.getDato());
        }
        else {
            System.out.println("No se encontró el dato.");
        }
    }
}
