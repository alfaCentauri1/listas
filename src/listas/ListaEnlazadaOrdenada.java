package listas;

public class ListaEnlazadaOrdenada extends ListaEnlazada{
    public ListaEnlazadaOrdenada() {
        super();
    }

    /**
     * Inserta un dato ordenando la lista.
     * @param dato Tipo int.
     **/
    public ListaEnlazada insertar(int dato){
        Nodo nuevo = new Nodo(dato);
        if ( this.getPrimero() == null ){
            //this.setPrimero(nuevo);
            this.agregarInicio(dato);
        }
        else {
            if (dato <= this.getPrimero().getDato() ){
                nuevo.setReferencia(this.getPrimero());
                this.setPrimero(nuevo);
            }
            else {
                Nodo anterior, primero0 = this.getPrimero();
                anterior = primero0;
                while (primero0.getReferencia() != null && dato > primero0.getDato()){
                    anterior = primero0;
                    primero0 = primero0.getReferencia();
                }
                if( dato > primero0.getDato() )
                    anterior = primero0;

                nuevo.setReferencia(anterior.getReferencia());
                anterior.setReferencia(nuevo);
            }
        }
        return this;
    }
}
