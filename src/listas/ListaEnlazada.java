package listas;

public class ListaEnlazada implements Lista {
    private Nodo primero;

    public ListaEnlazada() {
        primero = null;
    }

    @Override
    public Lista agregarInicio(int dato) {
        Nodo nuevo = new Nodo(dato);
        nuevo.setReferencia(primero);
        primero = nuevo;
        return this;
    }

    @Override
    public void agregarFinal(int dato) {
        Nodo nodoFin = new Nodo(dato);
        nodoFin.setReferencia(null);
        if (primero != null){
            Nodo temporal;
            for ( temporal = primero; temporal.getReferencia() != null; temporal = temporal.getReferencia() );
            temporal.setReferencia(nodoFin);
        }
        else {
            primero = nodoFin;
        }
    }

    @Override
    public Nodo buscar(int dato) {
        Nodo encontrado = null, actual;
        for(actual = primero; actual != null; actual = actual.getReferencia()){
            if (dato == actual.getDato())
                encontrado = actual;
        }
        return encontrado;
    }

    @Override
    public boolean eliminar(int dato) {
        boolean encontrado = false;
        Nodo nodoActual = primero;
        Nodo nodoAnterior = null;
        while (nodoActual != null && !encontrado){
            encontrado = (nodoActual.getDato() == dato);
            if (!encontrado){
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getReferencia();
            }
        }
        if (nodoActual != null){
            if (nodoActual == primero){
                primero = nodoActual.getReferencia();
            }
            else {
                nodoAnterior.setReferencia(nodoActual.getReferencia());
            }
        }
        return encontrado;
    }

    @Override
    public void verLista() {
        System.out.println("La lista es: ");
        Nodo nodoActual;
        nodoActual = primero;
        while (nodoActual != null){
            System.out.println(nodoActual.getDato());
            nodoActual = nodoActual.getReferencia();
        }
    }

    public Nodo getPrimero() {
        return primero;
    }

    public void setPrimero(Nodo primero) {
        this.primero = primero;
    }
}
