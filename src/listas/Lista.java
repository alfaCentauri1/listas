package listas;

public interface Lista {
    Lista agregarInicio(int dato);
    void agregarFinal(int dato);
    public Nodo buscar(int dato);
    boolean eliminar(int dato);
    void verLista();
}
